window.checkedNetworks = null;

function logCheckedNetworks() {
	if(window.checkedNetworks) {
		console.group('network');
		for(var i=0;i<window.checkedNetworks.length;i++) {
			console.log(window.checkedNetworks[i].value);
		}
		console.groupEnd('network');
	}
}

function getCheckedNetworks() {
	var checked = [];

	if(window.checkedNetworks) {
		for(var i=0;i<window.checkedNetworks.length;i++) {
			checked.push(window.checkedNetworks[i].value);
		}
	}
	return checked;
}

function getSelectedSites() {
	var list = document.querySelector('ul.ms-list');
	var selected = [];

	if(list) {
		for(var i=0;i<list.querySelectorAll('li.ms-elem-selectable').length;i++) {
			if(list.querySelectorAll('li.ms-elem-selectable')[i].classList.contains('ms-selected')) {
				selected.push(i);
			}
		}
	}
	return selected;
}

var h = function(e) {
	if(e.target.name == 'network') {
		window.checkedNetworks = document.querySelectorAll('input[name="network"]:checked');
		/*
		 * logCheckedNetworks();
		 */
	}
}

document.addEventListener('change',h,false);

function setHeading(h) {
	var table = document.createElement('table');
	var row = document.createElement('tr');
	var cell = document.createElement('td');

	cell.setAttribute('colspan','3');
	cell.innerHTML = h;
	row.appendChild(cell);
	table.appendChild(row);
	document.querySelector('div.modal').appendChild(table);
	return table;
}

function printData() {
	var networks = getCheckedNetworks();
	var sites = getSelectedSites();

	for(key_ in windowSheetData) {
		for(key__ in windowSheetData[key_]) {
			console.groupCollapsed(key_ + ',' + key__);
			if(networks.indexOf(key__) >= 0) {
				var table = setHeading(key_ + ',' + key__);

				for(var i=0;i<windowSheetData[key_][key__].length;i++) {
					if(sites.indexOf(i) >= 0) {
						console.log(windowSheetData[key_][key__][i]);

						var row = document.createElement('tr');
						var cella = document.createElement('td');
						var cellb = document.createElement('td');
						var cellc = document.createElement('td');

						cella.innerHTML = windowSheetData[key_][key__][i]['s'];
						cellb.innerHTML = windowSheetData[key_][key__][i]['a'];
						cellc.innerHTML = windowSheetData[key_][key__][i]['r'];
						row.appendChild(cella);
						row.appendChild(cellb);
						row.appendChild(cellc);
						table.appendChild(row);
					}
				}
			}
			console.groupEnd(key_ + ',' + key__);
		}
	}
}