'''
Date: 3 September
Module: myxls.py
Author: Prateek Mehta
Contact: prateek.mehta1992@gmail.com
'''


import xlrd
import webbrowser
import re
import html

##############################################################################################################
folder = r'D:\Python33\python_2013\xls'
_infile = 'in.xls'
_outfile = 'out.html'
inlocation = '{}\\{}'.format(folder,_infile)
outlocation = '{}\\{}'.format(folder,_outfile)
frh = xlrd.open_workbook(inlocation)
sheets = frh.sheet_names()
sheetdata = {}
location_indices = []
##############################################################################################################
datepattern = re.compile(r'date:\s([0-9]+)/([0-9]+)/.*',re.I)
def write(line,fh): print(line,end='',file=fh)
def isdef(v,b=False):
	if(b): return '"%s"'%(v) if str(v) else 'undefined'
	else: return v if str(v) else 'undefined'
'''
def lmax(l):
	max = 0
	for e in l:
		if max < int(e): max = int(e)
	return max
'''
##############################################################################################################
for name in sheets:
	sheet = frh.sheet_by_name(name)
	rawdatetime = sheet.cell_value(2,1)
	match = re.search(datepattern,rawdatetime)
	sheetdata[str(match.group(1)).lstrip('0')] = sheet
##############################################################################################################
# http://www.python.org/dev/peps/pep-0008/#descriptive-naming-styles
# class vs. class_
for e,key_ in enumerate(sheetdata):
##############################################################################################################
	sheet = sheetdata[key_]
	i = 0
	locations = []
##############################################################################################################
	while(i < sheet.nrows):
		(p,n) = (sheet.cell_value(i,0),sheet.cell_value(i,1))
		i += 1
		prevpattern = re.compile(r'^[0-9]+')
		next_pattern_a = re.compile(r'^([a-z]+[\s]?[0-9]?).*$',re.I)
		next_pattern_b = re.compile(r'^([a-z]+[\.]?[\s]?-[\s]?[a-z]+[\s]?[0-9]?).*$',re.I)
		next_pattern_c = re.compile(r'^([a-z]+[\.]?[\s]+[a-z]+[\s]?[a-z]+[\s]?[a-z]+).*$',re.I)
		if re.search(prevpattern,str(p)):
			if e==0: location_indices.append(i-1)
			match_a = re.search(next_pattern_a,str(n))
			match_b = re.search(next_pattern_b,str(n))
			match_c = re.search(next_pattern_c,str(n))
			if(match_a and match_b):
				locations.append(str(match_b.group(1)).strip() + '~')
				continue
			if(match_a and match_c):
				locations.append(str(match_c.group(1)).strip() + '~')
				continue
			if(match_a):
				locations.append(str(match_a.group(1)).strip() + '~')
	sheetdata[key_] = {}
	sheetdata[key_]['locations'] = []
	sheetdata[key_]['locations'].extend(locations)
# for key_ in sheetdata: print('{}: '.format(key_).ljust(5), sheetdata[key_]['locations'])
# print('Indices: {}'.format(location_indices))
##############################################################################################################
# 3,4,5 VSAT-CANOPY
# 6,7,8 MPLS-RELIANCE
# 9,10,11 MPLS-BSNL
# Status, Availability, RoundTripDelay
'''
sheetdata[key_]:{ # key_ = date
	locations:[a,b,c,] # for location in date['locations']: pass
	data:{
		VSAT-CANOPY:[(a1,b1,c1),(a2,b2,c2),(a3,b3,c3),] # for s,a,r in date['data']['VSAT-CANOPY']: pass
		MPLS-RELIANCE:[(a1,b1,c1),(a2,b2,c2),(a3,b3,c3),] # for s,a,r in date['data']['MPLS-RELIANCE']: pass
		MPLS-BSNL:[(a1,b1,c1),(a2,b2,c2),(a3,b3,c3),] # for s,a,r in date['data']['MPLS-BSNL']: pass
	}
}
'''
data_a_name = 'VSAT-CANOPY'
data_b_name = 'MPLS-RELIANCE'
data_c_name = 'MPLS-BSNL'
data_a = [] # VSAT-CANOPY
data_b = [] # MPLS-RELIANCE
data_c = [] # MPLS-BSNL
for name in sheets:
	sheet = frh.sheet_by_name(name)
	for i in location_indices:
		a = (sheet.cell_value(i,2),sheet.cell_value(i,3),sheet.cell_value(i,4))
		b = (sheet.cell_value(i,5),sheet.cell_value(i,6),sheet.cell_value(i,7))
		c = (sheet.cell_value(i,8),sheet.cell_value(i,9),sheet.cell_value(i,10))
		data_a.append(a)
		data_b.append(b)
		data_c.append(c)
for key_ in sheetdata:
	sheetdata[key_]['data'] = {}
	sheetdata[key_]['data'][data_a_name] = data_a
	sheetdata[key_]['data'][data_b_name] = data_b
	sheetdata[key_]['data'][data_c_name] = data_c
##############################################################################################################
'''
for e in data_a:
	print('{}'.format(e))
	mycount += 1
	if mycount == 20: break
'''
##############################################################################################################
fwh = open(outlocation,'w+t')
head = \
'''
<head>

<style>
html,body,div {
	padding: 0px;
	margin: 0px;
}
body {
	position: absolute;
	top: 0px;
	left: 0px;
	right: 0px;
	bottom: 0px;
	background-color: rgba(0, 0, 0, 1.0);
	background: url("bg.jpg") no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
div.main {
	position: absolute;
	display: block;
	box-shadow: 0px 0px 20px rgba(200, 200, 200, 0.6);
	background: rgba(200, 200, 200, 0.6);
	border: 2px dashed white;
	width: 450px;
	height: 550px;
	border-radius: 4px;
	margin: auto auto;
	padding: 10px;
	top: 0px;
	bottom: 0px;
	left: 0px;
	right: 0px;
}
label {
	font-family: monospace;
	text-shadow: 0px 0px 10px rgba(50, 50, 50, 0.5);
	color: #FFF;
	font-size: 18px;
	font-weight: normal;
}
select {
	box-shadow: 0px 0px 10px gold;
	border: 1px solid white;
	font-family: monospace;
}
input[type="checkbox"] {
	margin-top: 2px;
}
.modal {
	display: block;
	background-color: #999;
	box-shadow: inset 0px 0px 5px black;
	z-index: 100;
	padding: 10px;
}
.modal table tr {
	background-color: #888;
}
table td {
	padding: 5px;
	border-radius: 2px;
	box-shadow: inset 0px 0px 3px black;
	font-family: monospace;
	text-shadow: 0px 0px 5px rgba(50, 50, 50, 0.75);
	color: #FFF;
	font-size: 16px;
	font-weight: normal;
}
table tr:first-child td {
	background-color: #555;
}
</style>

<link rel="shortcut icon" href="http://static.pixdip.com/favicon.ico">
<link rel="stylesheet" href="overlay.css">
<link rel="stylesheet" href="multi-select/css/multi-select.css">

<script src="overlay.js"></script>
<script src="jquery-2.0.3.min.js"></script>

<title>myxls</title>

</head>
'''
write(head,fwh)
write('<body>',fwh)
write('<div class="main">',fwh)
keys = list(sheetdata.keys())
set_ = sheetdata[keys[0]]['locations']

'''
tkinter code
'''

print('Duplicates: ', list(set([x for x in set_ if set_.count(x) > 1])))
print('\n')

write('<label>Select Site </label><br><br>',fwh)
write('<select multiple="multiple" id="my-select" name="my-select[]">',fwh)
for i,location in enumerate(sheetdata[keys[0]]['locations']):
	write('<option value="{0}">{1}</option>'.format(i,location),fwh)
write('</select><hr>',fwh)
##############################################################################################################
write('<label>Select Day from </label>',fwh)
write('<select id="from">',fwh)
for i,day in enumerate(sorted([int(k) for k in keys])):
	write('<option value="{0}">{1}</option>'.format(i,day),fwh)
write('</select><label> to </label>',fwh)
write('<select id="to">',fwh)
for i,day in enumerate(sorted([int(k) for k in keys])):
	write('<option value="{0}">{1}</option>'.format(i,day),fwh)
write('</select><hr><label>Select Network </label><br><br>',fwh)
##############################################################################################################
##############################################################################################################
scriptA = \
'''
<script>
;(function() {
	var networks = ['VSAT-CANOPY','MPLS-RELIANCE','MPLS-BSNL','ALL'];

	for(var i=0;i<networks.length;i++) {
		var checkboxInput = document.createElement('input');
		var checkboxRow = document.createElement('div');

		checkboxInput.setAttribute('type','checkbox');
		checkboxInput.setAttribute('name','network');
		checkboxInput.setAttribute('value',networks[i]);

		checkboxRow.innerHTML = '<label>' + networks[i] + '</label>';
		checkboxRow.appendChild(checkboxInput);

		document.querySelector('div.main').appendChild(checkboxRow);

		if(networks[i] == 'ALL') {
			checkboxRow.style.visibility = 'hidden';
		}
	}

	document.querySelector('div.main').appendChild(document.createElement('hr'));

	document.addEventListener('change',
		function(e) {
			var checkboxes = document.querySelectorAll('input[type="checkbox"]');

			if(e.target.tagName == 'INPUT' && e.target.value == 'ALL' && e.target.checked) {
				for(var i=0;i<checkboxes.length;i++) {
					if(checkboxes[i].value != 'ALL') {
						checkboxes[i].checked = false;
					}
				}
			}

			if(e.target.tagName == 'INPUT' && e.target.value != 'ALL' && e.target.checked) {
				for(var i=0;i<checkboxes.length;i++) {
					if(checkboxes[i].value == 'ALL') {
						checkboxes[i].checked = false;
					}
				}
			}
		},false);
})();
</script>
'''
##############################################################################################################
scriptB = \
'''
<script>
windowSheetData = {};
;(function() {
	var dateKeys = [%s];
	var networks = ['VSAT-CANOPY','MPLS-RELIANCE','MPLS-BSNL'];
	var sheetData = {};

	for(var i=0;i<dateKeys.length;i++) {
		sheetData[dateKeys[i].toString()] = {};
		for(var j=0;j<networks.length;j++) {
			/* 
			 * var hash = {};
			 * hash.s = '';
			 * hash.s = '';
			 * hash.s = '';
			 */
			sheetData[dateKeys[i]][networks[j]] = [];
		}
	}
	windowSheetData = sheetData;
})();
/*
 * console.log(windowSheetData);
 */
</script>
'''
dateKeys = ''
for key_ in sheetdata:
	dateKeys += '"'
	dateKeys += key_
	dateKeys += '",'
	for key__ in sheetdata[key_]['data']:
		pass
		'''
		mycount = 0
		for (s,a,r) in sheetdata[key_]['data'][key__]:
			print('{},{}'.format(key_,key__))
			s = isdef(s,True)
			a = isdef(a)
			r = isdef(r)
			print('{},{},{}'.format(s,a,r))
			mycount += 1
			if mycount == 10: break
		print('\n')
		'''
##############################################################################################################
##############################################################################################################
write(scriptA,fwh)
write(scriptB%(dateKeys),fwh)
write('<script>',fwh)
for key_ in sheetdata:
	for key__ in sheetdata[key_]['data']:
		myrow = '['
		for (s,a,r) in sheetdata[key_]['data'][key__]:
			s = isdef(s,True)
			a = isdef(a)
			r = isdef(r)
			myrow += '{"s":%s,"a":%s,"r":%s},'%(s,a,r)
		myrow += ']'
		write('windowSheetData[{}]["{}"] = {};\n'.format(key_,key__,myrow),fwh)
write('</script>',fwh)
write('<script src="multi-select/js/jquery.multi-select.js"></script>',fwh)
write('<script>$("#my-select").multiSelect();</script>',fwh)
write('<script src="selected.js"></script>',fwh)
scriptC = \
'''
<script>
;(function() {
	var print = document.createElement('button');

	print.setAttribute('id','print');
	print.innerHTML = 'Print';
	print.style.marginLeft = '-1px';
	print.addEventListener('click',
	function(e) {
		printData();
		var main = document.querySelector('div.main');

		main.style.visibility = 'hidden';
	},false);

	document.querySelector('div.main').appendChild(print);
})();
</script>
'''
write(scriptC,fwh)
write('</div>',fwh)
write('<div class="modal"></div>',fwh)
scriptD = \
'''
<script>
;(function() {
	var modal = document.querySelector('div.modal');
	var main = document.querySelector('div.main');

	var observer = new MutationObserver(function(mutations) {
		console.groupCollapsed('MutationObserver');
		mutations.forEach(function(m) {
			console.log(m);
		});
		console.groupEnd('MutationObserver');
	});
	var config = {attributes:false, childList:true, characterData:true};
	observer.observe(modal,config);
	/*
	 * observer.disconnect();
	 */

	modal.addEventListener('click',
		function() {
			var tables = document.querySelectorAll('div.modal table');

			if(tables.length > 0) {
				$('div.modal').animate(
					{height:'0px',},
					3000,
					function() {
						modal.innerHTML = '';
						modal.removeAttribute('style');
						main.style.visibility = 'visible';
					}
				);
			}
		},false);
})();
</script>
'''
write(scriptD,fwh)
write('</body>',fwh)
fwh.close()
##############################################################################################################
webbrowser.open_new(outlocation)