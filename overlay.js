function removerOverlay() {
	if(document.querySelectorAll('div.overlay').length > 0) {
		var overlay = document.querySelector('div.overlay');

		overlay.parentNode.removeChild(overlay);
	}
}

window.onload = function() {
	var overlay = document.createElement('div');
	var myalert = document.createElement('div');
	var close = document.createElement('div');

	overlay.classList.add('overlay');
	myalert.classList.add('alert');
	close.classList.add('close');
	close.setAttribute('title','Close');
	close.innerHTML = 'x';

	var innerHTML = '<input id="username" type="text" placeholder=" Username">';
	innerHTML += '<input id="password" type="password" placeholder=" Password">';
	innerHTML += '<button id="submit">Submit</button>';

	myalert.innerHTML = innerHTML;
	myalert.appendChild(close);

	close.onclick = function() {
		var parent = this.parentNode;

		parent.parentNode.removeChild(parent);
		removerOverlay();
		/*
		 * window.close();
		 */
	}

	document.querySelector('body').appendChild(overlay);
	document.querySelector('body').appendChild(myalert);

	var button = document.querySelector('button#submit');

	button.addEventListener('click',
		function(e) {
			/*
			 * e.preventDefault();
			 */
			var username = document.querySelector('input#username');
			var password = document.querySelector('input#password');

			if(username.value.length > 0 && password.value.length > 0) {
				if(username.value == 'ntpc' && password.value == 'ntpc') {
					var parent = button.parentNode;

					parent.parentNode.removeChild(parent);
					removerOverlay();
				}
			}
		},false);
}